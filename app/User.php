<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function avatar($size = 45)
    {
        return 'https://www.gravatar.com/avatar/' . md5($this->email) . '?s='. $size .'&d=mm';
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();

        $array['avatar'] = $this->avatar(25);
        $array['country'] = $this->country;

        return $array;
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
