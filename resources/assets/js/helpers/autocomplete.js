import autocomplete from 'autocomplete.js'
import algolia from 'algoliasearch'

var index = algolia('RMY4FIDHLA', 'a5a6e8bd27a8755650306c3ff3da217b')

export const userautocomplete = (selector) => {
    index = index.initIndex('users')

    return autocomplete(selector, {
        hint: true
    }, {
        source: autocomplete.sources.hits(index, { hitsPerPage: 10 }),
        displayKey: 'name',
        templates: {
            suggestion (suggestion) {
                return '<img src="'+suggestion.avatar+'" alt="" /> &nbsp; <span> '+suggestion._highlightResult.name.value+'</span> <span class="pull-right">'+ suggestion.country.name +'</span>';
            },
            empty: '<div class="aa-empty">No people found.</div>'
        }
    })
}